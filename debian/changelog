spell (1.0-24co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 22 Feb 2021 02:28:22 +0000

spell (1.0-24) unstable; urgency=low

  * Support also aspell (Closes: #381511)
  * Removing some format only changes in 01-conglomeration.dpatch

 -- Giacomo Catenazzi <cate@debian.org>  Wed, 12 Aug 2009 21:01:58 +0200

spell (1.0-23) unstable; urgency=low

  * Adopting the package (Closes: #540590)
  * Added Homepage field in debian/control
  * Added again debian/watch

 -- Giacomo Catenazzi <cate@debian.org>  Wed, 12 Aug 2009 13:28:25 +0200

spell (1.0-22) unstable; urgency=low

  * Updating package to standards version 3.8.2.
  * Removing vcs fields.
  * Orphaning package.

 -- Daniel Baumann <daniel@debian.org>  Sun, 09 Aug 2009 01:09:08 +0200

spell (1.0-21) unstable; urgency=low

  * Removing watch file.
  * Reverting config.guess and config.sub to upstream.
  * Removing useless whitespaces in changelog file.
  * Upgrading package to debhelper 7.
  * Upgrading package to standards 3.8.0.
  * Removing autotools-dev from build-depends, not needed.
  * Adding vcs fields in control file.
  * Rewriting copyright file in machine-interpretable format.
  * Removing useless docs debhelper file.
  * Updating dpatch list file.
  * Reordering rules file.

 -- Daniel Baumann <daniel@debian.org>  Sat,  8 Nov 2008 12:23:00 +0100

spell (1.0-20) unstable; urgency=low

  * Applying patch from Chris Lamb <chris@chris-lamb.co.uk> to update
   01-conglomeration.dpatch (Closes: #441748).

 -- Daniel Baumann <daniel@debian.org>  Mon,  5 May 2008 07:06:00 +0100

spell (1.0-19) unstable; urgency=low

  * Bumped policy version.
  * Don't hide make errors in clean target of rules.

 -- Daniel Baumann <daniel@debian.org>  Sun, 23 Dec 2007 18:01:00 +0100

spell (1.0-18) unstable; urgency=low

  * Minor cleanups.
  * Bumped package to debhelper 5.

 -- Daniel Baumann <daniel@debian.org>  Tue,  1 May 2007 13:18:00 +0200

spell (1.0-17) unstable; urgency=low

  * New email address.
  * Updated policy version.
  * Using dpatch for upstream modifications now.

 -- Daniel Baumann <daniel@debian.org>  Fri,  8 Sep 2006 20:53:00 +0200

spell (1.0-16) unstable; urgency=low

  * Applied patch to fix pin/pout misstake (Closes: #335235).

 -- Daniel Baumann <daniel.baumann@panthera-systems.net>  Sat, 22 Oct 2005 21:17:00 +0200

spell (1.0-15) unstable; urgency=low

  * New maintainer (Closes: #330244).
  * Redone debian/ using newer debhelper-templates.
  * Added INFO-DIR-SECTION and INFO-DIR-SECTION to spell.texi/spell.info
  * Added build-depends for missing texinfo.
  * Added watch file.

 -- Daniel Baumann <daniel.baumann@panthera-systems.net>  Mon,  3 Oct 2005 16:40:00 +0200

spell (1.0-14) unstable; urgency=low

  * Orphaning spell.
  * Adding description of spell in manpage (Closes: Bug#208940)

 -- David Frey <david@eos.lugs.ch>  Mon, 26 Sep 2005 23:57:55 +0200

spell (1.0-13) unstable; urgency=low

  * Applied patch from Kaare Hviid which fixes the following
    Thanks to Kaare for the patch.
    * fix getopt_long for proper parsing in libc6,
    * Applied a bug fix from Luca Saiu:
      http://mail.gnu.org/archive/html/bug-gnu-utils/2003-07/msg00345.html,
    * added --ispell-dictionary to specify an ispell dictionary.
  * do not set link to /usr/doc in postinst (fixes lintian bug)
  * ignore SIGCHLD.
    Rewrote part of the select-logic in parent to simplify parsing
    of spell's output.
    This removes the dependency on ibritisch since the error output
    is parsed correctly. (closes: #158514)

 -- David Frey <david@eos.lugs.ch>  Sun, 11 Jan 2004 17:45:08 +0100

spell (1.0-12) unstable; urgency=low

  * recompile for unstable,
  * linted the package,
  * added depends on ibritish and ispell-dictionary (closes: #108351)

 -- David Frey <dfrey@debian.org>  Sun, 25 Aug 2002 17:26:48 +0200

spell (1.0-11) unstable; urgency=low

  * fixed priority.

 -- David Frey <dfrey@debian.org>  Sat, 10 Feb 2001 23:57:47 +0100

spell (1.0-10) unstable; urgency=low

  * recompile for woody.

 -- David Frey <dfrey@debian.org>  Sun,  7 Jan 2001 23:09:10 +0100

spell (1.0-9) unstable; urgency=low

  * made package FHS compliant.

 -- David Frey <dfrey@debian.org>  Sun, 10 Oct 1999 17:18:51 +0200

spell (1.0-8) unstable; urgency=low

  * recompile for potato.
  * renamed changelog into changelog.Debian.

 -- David Frey <dfrey@debian.org>  Sat, 15 May 1999 21:18:13 +0200

spell (1.0-7) unstable; urgency=low

  * cleaned up manpage from template leftovers.

 -- David Frey <dfrey@debian.org>  Sat, 15 May 1999 21:18:01 +0200

spell (1.0-6) frozen unstable; urgency=medium

  * fixed Bug#22943: spell: Segmentation fault every time used
    (Faulty pointer arithmetics in str.c)

 -- David Frey <dfrey@debian.org>  Sun,  7 Jun 1998 23:04:49 +0200

spell (1.0-5) unstable; urgency=low

  * changed maintainer address to official Debian address.
  * fixed bugs reported by debian: old FSF address, md5sums starting with dot.

 -- David Frey <dfrey@debian.org>  Wed, 11 Feb 1998 00:17:31 +0100

spell (1.0-4) unstable; urgency=low

  * Close Bug#16479: 'spell: debian/rules assumes post{inst,rm} are
    executable' by installing post{inst,rm} with install and not
    copying it.

 -- David Frey <david@eos.lugs.ch>  Sat,  3 Jan 1998 21:57:25 +0100

spell (1.0-3) unstable; urgency=low

  * got rid of debstd call
  * md5sum doesn't include DEBIAN anymore.

 -- David Frey <david@eos.lugs.ch>  Tue, 16 Dec 1997 20:24:43 +0100

spell (1.0-2) unstable; urgency=low

  * New maintainer
  * cleaned up debmake stuff

 -- David Frey <dFrey@debian.org>  Mon,  3 Nov 1997 21:34:59 +0100

spell (1.0-1) unstable; urgency=low

  * Initial Release.
  * Added a simple manpage by using the template provided by deb-make.

 -- Dominik Kubla <debian@debian.org>  Fri, 28 Feb 1997 14:54:43 +0100
